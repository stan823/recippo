package com.stanislawbrzezinski.viewmodels;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.Observer;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;

import com.stanislawbrzezinski.live_data.DialogFragmentsLiveData;
import com.stanislawbrzezinski.models.statics.DialogFragmentTypes;

/**
 * Created by stanislawbrzezinski on 04/01/2018.
 */

class MainActivityDialogFragmentsViewModel implements IMainActivityLoginDialogFragmentsViewModel {

    private static MainActivityDialogFragmentsViewModel instance;

    public static MainActivityDialogFragmentsViewModel getInstance(DialogFragmentsLiveData dialogFragmentsLiveData) {
        if (instance == null) {
            instance = new MainActivityDialogFragmentsViewModel(dialogFragmentsLiveData);
        }
        return instance;
    }

    @NonNull
    private DialogFragmentsLiveData dialogFragmentsLiveData;

    private MainActivityDialogFragmentsViewModel(DialogFragmentsLiveData dialogFragmentsLiveData) {
        this.dialogFragmentsLiveData = dialogFragmentsLiveData;
    }

    @MainThread
    public void observe(@NonNull LifecycleOwner owner, @NonNull Observer<DialogFragmentTypes> observer) {
        dialogFragmentsLiveData.observe(owner, observer);
    }


}
