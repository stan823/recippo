package com.stanislawbrzezinski.viewmodels;


import com.stanislawbrzezinski.live_data.BackFragmentLiveData;
import com.stanislawbrzezinski.live_data.CurrentlyOpenScreenLiveData;
import com.stanislawbrzezinski.live_data.DialogFragmentsLiveData;
import com.stanislawbrzezinski.live_data.LoginLiveData;

/**
 * Created by stanislawbrzezinski on 24/12/2017.
 */

public class ViewModelsFactory {

    private final static CurrentlyOpenScreenLiveData currentlyOpenScreenLiveData=CurrentlyOpenScreenLiveData.getInstance();
    private final static BackFragmentLiveData backFragmentLiveData=BackFragmentLiveData.getInstance();
    private final static LoginLiveData loginLiveData=LoginLiveData.getInstance();
    private final static DialogFragmentsLiveData dialogFragmentsLiveData=DialogFragmentsLiveData.getInstance();

    public static MainActivitScreenStateViewModel getMainActivityCurrentScreenViewModel(){
        return MainActivitScreenStateViewModel.getInstance(CurrentlyOpenScreenLiveData.getInstance(),
                BackFragmentLiveData.getInstance());
    }

    public static MainFragmentViewModel getMainFragmentViewModel(){
        return MainFragmentViewModel.getInstance(currentlyOpenScreenLiveData,backFragmentLiveData);
    }

    public static IMainActivityLoginDialogFragmentsViewModel getMainActivityDialogFragmentsModel(){
        return MainActivityDialogFragmentsViewModel.getInstance(dialogFragmentsLiveData);
    }
}
