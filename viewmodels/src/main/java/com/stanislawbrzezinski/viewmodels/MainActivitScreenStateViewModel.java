package com.stanislawbrzezinski.viewmodels;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.Observer;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;

import com.stanislawbrzezinski.live_data.BackFragmentLiveData;
import com.stanislawbrzezinski.live_data.CurrentlyOpenScreenLiveData;
import com.stanislawbrzezinski.models.statics.CurrentScreenState;
import com.stanislawbrzezinski.models.statics.FragmentsType;

/**
 * Created by stanislawbrzezinski on 24/12/2017.
 */

public class MainActivitScreenStateViewModel {
    private static MainActivitScreenStateViewModel INSTANCE = null;
    private BackFragmentLiveData backFragmentLiveData;
    private CurrentlyOpenScreenLiveData currentlyOpenScreenLiveData;
    private CurrentScreenState prevState = CurrentScreenState.CENTER_FROM_LEFT;
    private FragmentsType prevFragmentType = FragmentsType.FRAGMENT_SOCIAL;

    public static MainActivitScreenStateViewModel getInstance(CurrentlyOpenScreenLiveData currentlyOpenScreenLiveData,
                                                              BackFragmentLiveData backFragmentLiveData) {
        if (INSTANCE == null) {
            INSTANCE = new MainActivitScreenStateViewModel(currentlyOpenScreenLiveData, backFragmentLiveData);
        }
        return INSTANCE;
    }

    protected MainActivitScreenStateViewModel(CurrentlyOpenScreenLiveData currentlyOpenScreenLiveData,
                                              BackFragmentLiveData backFragmentLiveData) {
        this.currentlyOpenScreenLiveData = currentlyOpenScreenLiveData;
        this.backFragmentLiveData = backFragmentLiveData;
    }

    public FragmentsType getCurrentBackFragmentType(){
        return backFragmentLiveData.getValue();
    }
    @MainThread
    public void observe(@NonNull LifecycleOwner owner, @NonNull Observer<CurrentScreenState> observer) {
        if (currentlyOpenScreenLiveData.getValue() != prevState) {
            currentlyOpenScreenLiveData.observe(owner, observer);
        }
    }

    @MainThread
    public void observeBackFragment(@NonNull LifecycleOwner owner, @NonNull Observer<FragmentsType> observer) {
        if (prevFragmentType != backFragmentLiveData.getValue()) {
            backFragmentLiveData.observe(owner, observer);
        }
    }

    public void openRightFragment(FragmentsType fragmentsType){

    }

    public void openLeftFragment(FragmentsType fragmentsType){

    }

    public void changeState(CurrentScreenState newState) {
        currentlyOpenScreenLiveData.postValue(newState);
    }
}
