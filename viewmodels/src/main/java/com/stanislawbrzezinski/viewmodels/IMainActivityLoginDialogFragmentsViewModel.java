package com.stanislawbrzezinski.viewmodels;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.Observer;
import android.support.annotation.NonNull;

import com.stanislawbrzezinski.models.statics.DialogFragmentTypes;

/**
 * Created by stanislawbrzezinski on 04/01/2018.
 */

public interface IMainActivityLoginDialogFragmentsViewModel {
    void observe(@NonNull LifecycleOwner owner, @NonNull Observer<DialogFragmentTypes> observer);
}
