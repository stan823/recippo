package com.stanislawbrzezinski.viewmodels;

import com.stanislawbrzezinski.live_data.BackFragmentLiveData;
import com.stanislawbrzezinski.live_data.CurrentlyOpenScreenLiveData;
import com.stanislawbrzezinski.live_data.DialogFragmentsLiveData;
import com.stanislawbrzezinski.models.statics.CurrentScreenState;
import com.stanislawbrzezinski.models.statics.DialogFragmentTypes;
import com.stanislawbrzezinski.models.statics.FragmentsType;

/**
 * Created by stanislawbrzezinski on 27/12/2017.
 */

public class MainFragmentViewModel {
    private static MainFragmentViewModel instance;
    private BackFragmentLiveData backFragmentLiveData;
    private CurrentlyOpenScreenLiveData currentlyOpenScreenLiveData;

    public static MainFragmentViewModel getInstance(CurrentlyOpenScreenLiveData currentlyOpenScreenLiveData,BackFragmentLiveData backFragmentLiveData) {
        if (instance==null){
            instance=new MainFragmentViewModel(backFragmentLiveData, currentlyOpenScreenLiveData);
        }
        return instance;
    }

    private MainFragmentViewModel(BackFragmentLiveData backFragmentLiveData, CurrentlyOpenScreenLiveData currentlyOpenScreenLiveData) {
        this.backFragmentLiveData = backFragmentLiveData;
        this.currentlyOpenScreenLiveData = currentlyOpenScreenLiveData;
    }

    public void onInboxClicked(){
        DialogFragmentsLiveData.getInstance().postValue(DialogFragmentTypes.LOGIN);
        BackFragmentLiveData.getInstance().setValue(FragmentsType.FRAGMENT_INBOX);
        CurrentlyOpenScreenLiveData.getInstance().postValue(CurrentScreenState.LEFT_OPEN);
    }

    public void onSocialClicked(){
        BackFragmentLiveData.getInstance().setValue(FragmentsType.FRAGMENT_SOCIAL);
        CurrentlyOpenScreenLiveData.getInstance().postValue(CurrentScreenState.LEFT_OPEN);
    }

    public void onBasketClicked() {
        BackFragmentLiveData.getInstance().setValue(FragmentsType.FRAGMENT_BASKET);
        CurrentlyOpenScreenLiveData.getInstance().postValue(CurrentScreenState.RIGTH_OPEN);
    }

    public void onProfileClicked() {
        BackFragmentLiveData.getInstance().setValue(FragmentsType.FRAGMENT_PROFILE);
        CurrentlyOpenScreenLiveData.getInstance().postValue(CurrentScreenState.RIGTH_OPEN);
    }
}
