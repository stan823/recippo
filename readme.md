# Recippo - Android

## Description

App, already published in Early Access in PlayStore - Creates social network for people who would like to share cooking recipes.

[PlayStore link](https://play.google.com/store/apps/details?id=com.stanislawbrzezinski.recippo2)