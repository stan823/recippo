package com.stanislawbrzezinski.live_data;

import android.arch.lifecycle.MutableLiveData;

import com.stanislawbrzezinski.models.statics.CurrentScreenState;

/**
 * Created by stanislawbrzezinski on 23/12/2017.
 */

public class CurrentlyOpenScreenLiveData extends MutableLiveData<CurrentScreenState> {
    private static final CurrentlyOpenScreenLiveData ourInstance = new CurrentlyOpenScreenLiveData();

    public static CurrentlyOpenScreenLiveData getInstance() {
        return ourInstance;
    }

    private CurrentlyOpenScreenLiveData() {
        postValue(CurrentScreenState.CENTER_FROM_LEFT);
    }
}
