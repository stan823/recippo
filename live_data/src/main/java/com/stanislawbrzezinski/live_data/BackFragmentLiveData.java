package com.stanislawbrzezinski.live_data;


import android.arch.lifecycle.MutableLiveData;

import com.stanislawbrzezinski.models.statics.FragmentsType;

/**
 * Created by stanislawbrzezinski on 24/12/2017.
 */

public class BackFragmentLiveData extends MutableLiveData<FragmentsType> {
    private static BackFragmentLiveData INSTANCE=new BackFragmentLiveData();

    public static BackFragmentLiveData getInstance() {
        return INSTANCE;
    }

    private BackFragmentLiveData(){
        this.setValue(FragmentsType.FRAGMENT_INBOX);
    }
}
