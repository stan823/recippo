package com.stanislawbrzezinski.live_data;


import android.arch.lifecycle.MutableLiveData;

import com.stanislawbrzezinski.models.statics.DialogFragmentTypes;

/**
 * Created by stanislawbrzezinski on 04/01/2018.
 */

public class DialogFragmentsLiveData extends MutableLiveData<DialogFragmentTypes> {
    private final static DialogFragmentsLiveData instance=new DialogFragmentsLiveData();

    public static DialogFragmentsLiveData getInstance() {
        return instance;
    }

    private DialogFragmentsLiveData(){}
}
