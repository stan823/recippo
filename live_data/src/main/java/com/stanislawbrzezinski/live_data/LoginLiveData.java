package com.stanislawbrzezinski.live_data;

import android.arch.lifecycle.MutableLiveData;

import com.stanislawbrzezinski.models.statics.LoginLiveDataModel;

/**
 * Created by stanislawbrzezinski on 04/01/2018.
 */

public class LoginLiveData extends MutableLiveData<LoginLiveDataModel> {
    private static LoginLiveData instance=new LoginLiveData();

    public static LoginLiveData getInstance() {
        return instance;
    }

    public LoginLiveData() {
        this.postValue(new LoginLiveDataModel());
    }
}
