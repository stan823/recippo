package com.stanislawbrzezinski.models.statics;

/**
 * Created by stanislawbrzezinski on 04/01/2018.
 */

public class LoginLiveDataModel {
    private String password="";
    private String email="";

    public LoginLiveDataModel(String password, String email) {
        this.password = password;
        this.email = email;
    }

    public LoginLiveDataModel() {
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
