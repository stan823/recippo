package com.stanislawbrzezinski.models.statics;

/**
 * Created by stanislawbrzezinski on 24/12/2017.
 */

public enum FragmentsType {
    FRAGMENT_SOCIAL,
    FRAGMENT_INBOX,
    FRAGMENT_BASKET,
    FRAGMENT_PROFILE
}
