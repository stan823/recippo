package com.stanislawbrzezinski.models.statics;

/**
 * Created by stanislawbrzezinski on 23/12/2017.
 */

public enum CurrentScreenState {
    CENTER_FROM_LEFT,
    LEFT_OPEN,
    CENTER_FROM_RIGHT, RIGTH_OPEN
}
