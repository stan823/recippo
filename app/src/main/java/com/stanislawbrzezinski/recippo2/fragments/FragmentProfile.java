package com.stanislawbrzezinski.recippo2.fragments;

import android.view.View;

import com.stanislawbrzezinski.recippo2.R;

/**
 * Created by stanislawbrzezinski on 18/12/2017.
 */

public class FragmentProfile extends BaseFragment {
    public static FragmentProfile getFragment(){
        return new FragmentProfile();
    }
    public FragmentProfile() {
    }

    @Override
    protected void addWidgetListeners(View view) {

    }

    @Override
    protected void setUpWidgets(View view) {

    }

    @Override
    protected int createLayoutResource() {
        return R.layout.fragment_profile;
    }
}
