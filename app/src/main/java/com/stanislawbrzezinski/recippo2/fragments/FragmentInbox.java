package com.stanislawbrzezinski.recippo2.fragments;

import android.view.View;

import com.stanislawbrzezinski.recippo2.R;

/**
 * Created by stanislawbrzezinski on 24/12/2017.
 */

public class FragmentInbox extends BaseFragment{
    public static BaseFragment getFragment(){
        return new FragmentInbox();
    }

    public FragmentInbox() {
    }

    @Override
    protected void addWidgetListeners(View view) {

    }

    @Override
    protected void setUpWidgets(View view) {

    }

    @Override
    protected int createLayoutResource() {
        return R.layout.fragment_inbox;
    }
}
