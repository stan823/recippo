package com.stanislawbrzezinski.recippo2.fragments;

import android.view.View;

import com.stanislawbrzezinski.recippo2.R;

/**
 * Created by stanislawbrzezinski on 18/12/2017.
 */

public class FragmentSocial extends BaseFragment {
    public static FragmentSocial getFragment(){
        return new FragmentSocial();
    }
    public FragmentSocial() {
    }

    @Override
    protected void addWidgetListeners(View view) {

    }

    @Override
    protected void setUpWidgets(View view) {

    }

    @Override
    protected int createLayoutResource() {
        return R.layout.fragment_social;
    }
}
