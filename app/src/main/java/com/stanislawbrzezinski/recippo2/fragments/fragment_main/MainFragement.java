package com.stanislawbrzezinski.recippo2.fragments.fragment_main;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.stanislawbrzezinski.recippo2.R;
import com.stanislawbrzezinski.recippo2.fragments.BaseFragment;
import com.stanislawbrzezinski.viewmodels.MainFragmentViewModel;
import com.stanislawbrzezinski.viewmodels.ViewModelsFactory;

/**
 * Created by stanislawbrzezinski on 17/12/2017.
 */

public class MainFragement extends BaseFragment {

    private View socialButton;
    private View inboxButton;
    private MainFragmentViewModel topBarViewModel;
    private View profileIcon;
    private View basketIcon;
    private ViewPager viewPager;

    public static Fragment getFragment() {
        MainFragement fr = new MainFragement();
        return fr;
    }

    public MainFragement() {
    }

    @Override
    protected void addWidgetListeners(View view) {
        socialButton.setOnClickListener(v -> topBarViewModel.onSocialClicked());
        inboxButton.setOnClickListener(v -> topBarViewModel.onInboxClicked());
        profileIcon.setOnClickListener(v -> topBarViewModel.onProfileClicked());
        basketIcon.setOnClickListener(v -> topBarViewModel.onBasketClicked());
    }

    @Override
    protected void init() {
        setUpViewModels();
    }

    private void setUpViewModels() {
        topBarViewModel = ViewModelsFactory.getMainFragmentViewModel();
    }


    @Override
    protected void setUpWidgets(View view) {
        socialButton = view.findViewById(R.id.socialButton);
        inboxButton = view.findViewById(R.id.inboxButton);
        basketIcon = view.findViewById(R.id.basketIcon);
        profileIcon = view.findViewById(R.id.profileIcon);
        viewPager = view.findViewById(R.id.viewPager);

        TabLayout tabContainerView = view.findViewById(R.id.tabView);
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getFragmentManager());
        viewPager.setAdapter(viewPagerAdapter);
        tabContainerView.setupWithViewPager(viewPager);

    }

    @Override
    protected int createLayoutResource() {
        return R.layout.fragment_main;
    }
}
