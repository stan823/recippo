package com.stanislawbrzezinski.recippo2.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by stanislawbrzezinski on 17/12/2017.
 */

public abstract class BaseFragment extends Fragment {
    private View view;
    public BaseFragment() {
    }

    @Nullable
    @Override
    public final View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
         this.view=inflater.inflate(this.createLayoutResource(),null);
         setUpWidgets(view);
         addWidgetListeners(view);
         init();
         return view;
    }

    protected void init() {

    }

    protected abstract void addWidgetListeners(View view);

    protected abstract void setUpWidgets(View view);

    protected abstract int createLayoutResource();
}
