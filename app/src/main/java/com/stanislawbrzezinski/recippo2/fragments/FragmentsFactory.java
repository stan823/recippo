package com.stanislawbrzezinski.recippo2.fragments;

import com.stanislawbrzezinski.models.statics.FragmentsType;

/**
 * Created by stanislawbrzezinski on 24/12/2017.
 */

public class FragmentsFactory {
    public BaseFragment getFragment(FragmentsType type) {
        switch (type) {
            case FRAGMENT_INBOX:
                return FragmentInbox.getFragment();
            case FRAGMENT_SOCIAL:
                return FragmentSocial.getFragment();
            case FRAGMENT_BASKET:
                return FragmentBasket.getFragment();
            case FRAGMENT_PROFILE:
                return FragmentProfile.getFragment();
            default:
                return FragmentSocial.getFragment();

        }
    }
}
