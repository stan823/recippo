package com.stanislawbrzezinski.recippo2.fragments.fragment_main.subfragments;

import android.view.View;

import com.stanislawbrzezinski.recippo2.R;
import com.stanislawbrzezinski.recippo2.fragments.BaseFragment;

/**
 * Created by stanislawbrzezinski on 30/12/2017.
 */

public class FragmentRecent extends BaseFragment {
    public static FragmentRecent getFragment(){
        return new FragmentRecent();
    }
    @Override
    protected void addWidgetListeners(View view) {

    }

    @Override
    protected void setUpWidgets(View view) {

    }

    @Override
    protected int createLayoutResource() {
        return R.layout.fragment_recent;
    }
}
