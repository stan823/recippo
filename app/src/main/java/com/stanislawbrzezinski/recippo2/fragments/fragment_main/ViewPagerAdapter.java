package com.stanislawbrzezinski.recippo2.fragments.fragment_main;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;

import com.stanislawbrzezinski.recippo2.fragments.fragment_main.subfragments.FragmentFavourites;
import com.stanislawbrzezinski.recippo2.fragments.fragment_main.subfragments.FragmentFollowedRecipes;
import com.stanislawbrzezinski.recippo2.fragments.fragment_main.subfragments.FragmentRecent;

/**
 * Created by stanislawbrzezinski on 30/12/2017.
 */

class ViewPagerAdapter extends FragmentPagerAdapter {
    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return FragmentRecent.getFragment();
            case 1:
                return FragmentFollowedRecipes.getFragment();
            default:
                return FragmentFavourites.getFragment();
        }
    }


    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:return "Recent";
            case 1:return "Followed";
            default:return "Favourites";
        }
    }

    @Override
    public int getCount() {
        return 3;
    }
}
