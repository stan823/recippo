package com.stanislawbrzezinski.recippo2.fragments.fragment_main.subfragments;

import android.view.View;

import com.stanislawbrzezinski.recippo2.R;
import com.stanislawbrzezinski.recippo2.fragments.BaseFragment;
import com.stanislawbrzezinski.recippo2.fragments.FragmentBasket;

/**
 * Created by stanislawbrzezinski on 30/12/2017.
 */

public class FragmentFavourites extends BaseFragment {
    public static FragmentFavourites getFragment() {
        return new FragmentFavourites();
    }

    public FragmentFavourites() {
    }

    @Override
    protected void addWidgetListeners(View view) {

    }

    @Override
    protected void setUpWidgets(View view) {

    }

    @Override
    protected int createLayoutResource() {
        return R.layout.fragment_favourites;
    }
}
