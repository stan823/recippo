package com.stanislawbrzezinski.recippo2.activities.main_activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import com.stanislawbrzezinski.models.statics.DialogFragmentTypes;
import com.stanislawbrzezinski.recippo2.R;
import com.stanislawbrzezinski.recippo2.animators.MainFragmentCloseLeftAnimator;
import com.stanislawbrzezinski.recippo2.animators.MainFragmentSlideLeftAnimator;
import com.stanislawbrzezinski.recippo2.animators.MainFragmentSlideRightAnimator;
import com.stanislawbrzezinski.models.statics.CurrentScreenState;
import com.stanislawbrzezinski.recippo2.dialog_fragments.DialogFragmentLogin;
import com.stanislawbrzezinski.recippo2.fragments.BaseFragment;
import com.stanislawbrzezinski.recippo2.fragments.FragmentsFactory;
import com.stanislawbrzezinski.recippo2.fragments.fragment_main.MainFragement;
import com.stanislawbrzezinski.viewmodels.IMainActivityLoginDialogFragmentsViewModel;
import com.stanislawbrzezinski.viewmodels.MainActivitScreenStateViewModel;
import com.stanislawbrzezinski.viewmodels.ViewModelsFactory;

public class MainActivity extends AppCompatActivity {

    private View mainFragmentHolder;
    private ViewGroup backFRagment;
    private View closeButton;
    private View mainFragmentFrame;
    private MainActivitScreenStateViewModel currentScreenViewModel;
    private IMainActivityLoginDialogFragmentsViewModel dialogFragmentsViewModel;
    private FragmentsFactory fragmentsFactory = new FragmentsFactory();
    private View closeButtonRight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUpWidgehts();
        setUpListenres();
        attachMainFragment();
        setUpViewModels();
    }

    private void setUpViewModels() {
        currentScreenViewModel = ViewModelsFactory.getMainActivityCurrentScreenViewModel();
        dialogFragmentsViewModel =ViewModelsFactory.getMainActivityDialogFragmentsModel();
        currentScreenViewModel.observe(this, this::onCurrentStateChanged);
        dialogFragmentsViewModel.observe(this,this::onDialogFragmentsChanged);
    }

    private void onDialogFragmentsChanged(DialogFragmentTypes dialogFragmentTypes) {
        switch (dialogFragmentTypes){
            case LOGIN:
                DialogFragmentLogin.getDialogFragment().show(getFragmentManager(),"");
        }
    }


    private void onCurrentStateChanged(CurrentScreenState currentScreenState) {
        BaseFragment fragment = fragmentsFactory.getFragment(currentScreenViewModel.getCurrentBackFragmentType());
        getSupportFragmentManager().beginTransaction().replace(R.id.backFragment, fragment).commit();

        if (currentScreenState == CurrentScreenState.LEFT_OPEN) {
            new MainFragmentSlideLeftAnimator().start(this, mainFragmentFrame, closeButton, backFRagment);
        } else if (currentScreenState == CurrentScreenState.CENTER_FROM_LEFT) {
            new MainFragmentCloseLeftAnimator().start(this, mainFragmentFrame, closeButton, backFRagment);
        } else if (currentScreenState == CurrentScreenState.RIGTH_OPEN) {
            new MainFragmentSlideRightAnimator().start(this, mainFragmentFrame, closeButtonRight, backFRagment);
        } else {
            new MainFragmentCloseLeftAnimator().start(this, mainFragmentFrame, closeButtonRight, backFRagment);
        }
    }

    private void setUpListenres() {
        closeButton.setOnClickListener(this::onCloseClicked);
        closeButtonRight.setOnClickListener(this::onCloseRightClicked);
    }

    private void onCloseClicked(View view) {
        currentScreenViewModel.changeState(CurrentScreenState.CENTER_FROM_LEFT);
    }

    private void onCloseRightClicked(View view) {
        currentScreenViewModel.changeState(CurrentScreenState.CENTER_FROM_RIGHT);
    }

    private void attachMainFragment() {
        MainFragement mainFragement = (MainFragement) MainFragement.getFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.mainFragmentHolder, mainFragement, "a").commit();
    }

    private void setUpWidgehts() {
        backFRagment = findViewById(R.id.backFragment);
        mainFragmentHolder = findViewById(R.id.mainFragmentHolder);
        closeButton = findViewById(R.id.closeButton);
        closeButtonRight = findViewById(R.id.closeButtonRight);
        mainFragmentFrame = findViewById(R.id.mainFragmentFrame);

    }


}
