package com.stanislawbrzezinski.recippo2.animators;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.Activity;
import android.view.View;
import android.view.animation.DecelerateInterpolator;

import com.stanislawbrzezinski.recippo2.R;
import com.stanislawbrzezinski.recippo2.statics.Dimens;

/**
 * Created by stanislawbrzezinski on 20/12/2017.
 */

public class MainFragmentSlideRightAnimator {

    public void start(Activity activity, View mainFragmentFrame, View closeButton,View backFragment){

        float offset=activity.getResources().getDimension(R.dimen.mainFragmentOffset);
        ObjectAnimator animator = ObjectAnimator.ofFloat(mainFragmentFrame, View.X, 0, -(mainFragmentFrame.getMeasuredWidth()-offset));
        animator.setDuration(600);
        animator.setInterpolator(new DecelerateInterpolator());

        closeButton.setVisibility(View.VISIBLE);
        ObjectAnimator closeAnimator = ObjectAnimator.ofFloat(closeButton, View.ALPHA, 0, 1);
        closeAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                closeButton.setClickable(true);
            }
        });
        closeAnimator.setStartDelay(300);
        closeAnimator.setDuration(300);
        closeAnimator.setInterpolator(new DecelerateInterpolator());




        PropertyValuesHolder propertyValuesHolder = PropertyValuesHolder.ofFloat(View.SCALE_X, Dimens.BACK_FRAGMENT_SIZE, 1);
        PropertyValuesHolder propertyValuesHolder2 = PropertyValuesHolder.ofFloat(View.SCALE_Y, Dimens.BACK_FRAGMENT_SIZE, 1);
        PropertyValuesHolder propertyValuesHolder3 = PropertyValuesHolder.ofFloat(View.ALPHA, Dimens.BACK_FRAGMENT_ALPHA, 1);
        ObjectAnimator animator2 = ObjectAnimator.ofPropertyValuesHolder(backFragment, propertyValuesHolder,propertyValuesHolder3, propertyValuesHolder2);
        animator2.setDuration(400);
        animator2.setInterpolator(new DecelerateInterpolator());
        animator2.setStartDelay(100);

        animator2.start();
        closeAnimator.start();
        animator.start();
    }
}
