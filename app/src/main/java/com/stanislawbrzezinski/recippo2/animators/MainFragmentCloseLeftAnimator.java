package com.stanislawbrzezinski.recippo2.animators;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;

import com.stanislawbrzezinski.recippo2.R;
import com.stanislawbrzezinski.recippo2.fragments.FragmentSocial;
import com.stanislawbrzezinski.recippo2.statics.Dimens;

/**
 * Created by stanislawbrzezinski on 20/12/2017.
 */

public class MainFragmentCloseLeftAnimator {
    public void start(Activity activity, View mainFragmentFrame, View closeButton,View backFragment){

        ObjectAnimator animator = ObjectAnimator.ofFloat(mainFragmentFrame, View.X, mainFragmentFrame.getX(), 0);
        animator.setDuration(600);
        animator.setInterpolator(new DecelerateInterpolator());
        ObjectAnimator closeAnimator = ObjectAnimator.ofFloat(closeButton, View.ALPHA, 1, 0);

        closeAnimator.setDuration(300);
        closeAnimator.setInterpolator(new DecelerateInterpolator());
        closeAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                closeButton.setClickable(false);
            }
        });

        PropertyValuesHolder propertyValuesHolder = PropertyValuesHolder.ofFloat(View.SCALE_X, backFragment.getScaleX(), Dimens.BACK_FRAGMENT_SIZE);
        PropertyValuesHolder propertyValuesHolder2 = PropertyValuesHolder.ofFloat(View.SCALE_Y, backFragment.getScaleX(), Dimens.BACK_FRAGMENT_SIZE);
        PropertyValuesHolder propertyValuesHolder3 = PropertyValuesHolder.ofFloat(View.ALPHA, 1, Dimens.BACK_FRAGMENT_ALPHA);
        ObjectAnimator animator2 = ObjectAnimator.ofPropertyValuesHolder(backFragment, propertyValuesHolder, propertyValuesHolder3, propertyValuesHolder2);
        animator2.setInterpolator(new DecelerateInterpolator());
        animator2.setDuration(400);
        animator2.setStartDelay(100);
        animator2.start();
        closeAnimator.start();
        animator.start();
    }
}
