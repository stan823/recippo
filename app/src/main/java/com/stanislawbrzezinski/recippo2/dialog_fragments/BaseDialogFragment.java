package com.stanislawbrzezinski.recippo2.dialog_fragments;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by stanislawbrzezinski on 04/01/2018.
 */

public abstract class BaseDialogFragment extends DialogFragment {
    protected View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(createLayoutResource(), container, false);
        return view;
    }

    protected abstract int createLayoutResource();
}
