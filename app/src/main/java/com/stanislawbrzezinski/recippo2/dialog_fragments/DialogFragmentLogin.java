package com.stanislawbrzezinski.recippo2.dialog_fragments;

import com.stanislawbrzezinski.recippo2.R;

/**
 * Created by stanislawbrzezinski on 04/01/2018.
 */

public class DialogFragmentLogin extends BaseDialogFragment {
    public static DialogFragmentLogin getDialogFragment(){
        return new DialogFragmentLogin();
    }
    @Override
    protected int createLayoutResource() {
        return R.layout.dialog_login;
    }
}
