package com.stanislawbrzezinski.recippo2.statics;

/**
 * Created by stanislawbrzezinski on 18/12/2017.
 */

public class Dimens {
    public final static float BACK_FRAGMENT_SIZE=0.85F;
    public final static float BACK_FRAGMENT_ALPHA=0.8F;

}
